import {syncListYouTube} from '../../src/queuez/application/youtube/youtube-lib.js'

describe('Youtube', function() {

    it('should retrun array sync', () => {
        let oldList = [
            {
                id: 3
            },
            {
                id: 4
            },
            {
                id: 5
            },
            {
                id: 6
            },
            {
                id: 7
            }
        ];

        let newList = [
            {
                id: 1
            },
            {
                id: 2
            },
            {
                id: 3
            },
            {
                id: 8
            },
            {
                id: 9
            }
        ];

        let expectResult =  [
            {
                id: 3
            },
            {
                id: 8
            },
            {
                id: 9
            }
        ]
        expect(expectResult).toEqual(syncListYouTube(oldList, newList));
    });
});

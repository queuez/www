import Rx from '@reactivex/rxjs'

/**
 * My lib
 */
import {ajax} from './../lib/my-ajax'

const chatWSSubject = new Rx.Subject()

/**
 * [createRoomSubject description]
 * @type {Rx}
 */
const createRoomSubject = new Rx.Subject()

const creatingRoom = (data) => {
    return Rx.Observable.create(observer => {
        return ajax({
            url: `http://localhost:3000/rooms`,
            type: 'POST',
            data: JSON.stringify(data)
        })
        .then((data) => {
            data.status = 'success'
            observer.next(data)
            observer.complete()
        })
        .catch(() => {
            observer.next({
                status: 'error'
            })
            observer.complete()
        })
    })
}

const createRoom = createRoomSubject
    .switchMap(creatingRoom)
    .share()

const createRoomSuccess = createRoom
    .filter((response) => response.status === 'success')

const createRoomError = createRoom
    .filter((response) => response.status === 'error')

/**
 * [createRoomSubject description]
 * @type {Rx}
 */
const getRoomSubject = new Rx.Subject()

const gettingRooms = (data) => {
    return Rx.Observable.create(observer => {
        return ajax({
            url: `http://localhost:3000/rooms`,
            type: 'POST',
            data: JSON.stringify(data)
        })
        .then((data) => {
            data.status = 'success'
            observer.next(data)
            observer.complete()
        })
        .catch(() => {
            observer.next({
                status: 'error'
            })
            observer.complete()
        })
    })
}

const getRooms = getRoomSubject
    .filter((request) => request.case === 'all')
    .switchMap(gettingRooms)
    .share()

const getRoomsSuccess = getRooms
    .filter((response) => response.status === 'success')

const getRoomsError = getRooms
    .filter((response) => response.status === 'error')

const gettingMyRooms = ({userID}) => {
    return Rx.Observable.create(observer => {
        return ajax({
            url: `http://localhost:3000/rooms/${userID}`,
            type: 'GET'
        })
        .then((data) => {
            observer.next({
                status: 'success',
                data: data
            })
            observer.complete()
        })
        .catch(() => {
            observer.next({
                status: 'error'
            })
            observer.complete()
        })
    })
}

const getMyRooms = getRoomSubject
    .filter((request) => request.case === 'my')
    .switchMap(gettingMyRooms)
    .share()

const getMyRoomsSuccess = getMyRooms
    .filter((response) => response.status === 'success')

const getMyRoomsError = getMyRooms
    .filter((response) => response.status === 'error')

export {
    chatWSSubject,
    createRoomError,
    createRoomSubject,
    createRoomSuccess,
    getMyRoomsError,
    getMyRoomsSuccess,
    getRoomsError,
    getRoomSubject,
    getRoomsSuccess
}

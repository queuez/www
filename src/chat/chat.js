import _ from 'lodash'
import { Link } from 'react-router5'
import React from 'react'

/**
 *
 * Material UI
 */
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import {Tabs, Tab} from 'material-ui/Tabs';
import TextField from 'material-ui/TextField'

/**
 *
 * My lib
 */
import {
    chatWSSubject,
    createRoomError,
    createRoomSubject,
    createRoomSuccess,
    getMyRoomsError,
    getMyRoomsSuccess,
    getRoomsError,
    getRoomSubject,
    getRoomsSuccess} from './chat-rx'
import {iqzAuth} from './../lib/auth/auth-service'
import {socket} from './../lib/notification'
import {MeterialMixin} from './../lib/my-meterial-ui'
import {ReactorMixin} from './../lib/reactor'
import {router, routes} from './../lib/router'
import {WebSocketMixin} from './../lib/web-socket'

const Room = React.createClass({
    mixins: [ReactorMixin],
    getInitialState() {
        return {}
    },
    componentWillMount() {
        const myChat = chatWSSubject
            .filter(({chatId}) => chatId === this.props.value)

        this.on(myChat, (data) => {
            console.log('myChat', data);
        })
    },
    render() {
        return (
            <section>
                Hi
                <i className='material-icons'>face</i>
                <i className='glyphicon glyphicon-user'></i>
            </section>
        )
    }
})


export default React.createClass({
    mixins: [MeterialMixin, ReactorMixin, WebSocketMixin],
    getInitialState() {
        return {
            userID: iqzAuth.getUserID(),
            room: ''}
    },
    componentWillMount() {
        const ws = new WebSocket(`ws://localhost:3000?userID=${this.state.userID}`)
        this.onWs(ws)

        ws.onopen = function() {

        }

        ws.onmessage = function ({data}) {
            chatWSSubject.next(JSON.parse(data))
        }

        this.on(createRoomSuccess, (data) => {
            console.log('createRoomSuccess', data);
        })

        this.on(createRoomError, (data) => {
            console.log('createRoomError');
        })

        this.on(getMyRoomsSuccess, (data) => {
            console.log('getMyRoomsSuccess', data);
        })
    },
    componentDidMount() {
    },
    handleChange(state) {
        return (e) => {
            let setState = {}
            setState[`${state}`] = e.target.value
            this.setState(setState)
        }
    },
    handleCreateRoom (e) {
        console.log('handleCreateRoom');
        createRoomSubject.next({
            userid: this.state.userID,
            room: this.state.room
        })
    },
    handleGetMyRooms (e) {
        console.log('handleGetMyRooms');
        getRoomSubject.next({
            case: 'my',
            userID: this.state.userID
        })
    },
    render() {
        return (
            <Card className='chat-container'>
                <div className="input-group">
                    <input type="text" className="form-control" placeholder="Create new room"
                        onChange={this.handleChange('room')}
                    />
                    <span className="input-group-btn">
                        <button className="btn btn-default" type="button"
                            onMouseDown={this.handleCreateRoom}
                        >
                            <i className="glyphicon glyphicon-plus"></i>
                        </button>
                    </span>
                </div>
                <button className="btn btn-default" type="button"
                    onMouseDown={this.handleGetMyRooms}
                >
                    My Room
                </button>
                <Room />
            </Card>
        )
    }
})

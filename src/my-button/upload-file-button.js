import React from 'react'

/**
    * Material UI
    */
import IconButton from 'material-ui/IconButton'
import FileCloudUpload from 'material-ui/svg-icons/file/cloud-upload'

export default React.createClass({
    handleFileChange(e) {
        this.props.onFileChange(e.target.files[0]);
        if(this.props.imageId) {
            let reader = new FileReader();
            reader.onload = (e) => {
                document.getElementById(this.props.imageId).src = e.target.result;
            }
            reader.readAsDataURL(e.target.files[0]);
        }
    },
    render() {
        return (
            <IconButton style={{width: 36, height: 36, padding: 8}}
                className="upload-file-button-container">
                <FileCloudUpload/>
                <label className='button' htmlFor={this.props.id}>
                </label>
                <input className='none' id={this.props.id} name='file' type='file'
                    onChange={this.handleFileChange}
                />
            </IconButton>
        );
    }
})

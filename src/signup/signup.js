import _ from 'lodash'
import React from 'react'

/**
    * Material UI
    */
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton'
import RaisedButton from 'material-ui/RaisedButton'
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card'
import TextField from 'material-ui/TextField'

import * as MyAjax from './../lib/my-ajax'
import {MeterialMixin} from './../lib/my-meterial-ui'
import {ReactorMixin} from './../lib/reactor'
import {router} from './../lib/router'

let SignUpForm = React.createClass({
    mixins: [ReactorMixin],
    getInitialState() {
        return {email: '', password: '', confirmPassword: '', name: '', gender: 'true'}
    },
    componentWillMount() {
        this.on(MyAjax.signupSuccess, (response) => {
            router.navigate('login')
        })
    },
    handleChange(state) {
        return (e) => {
            let setState = {}
            setState[`${state}`] = e.target.value
            this.setState(setState)
        }
    },
    handleSubmit(e) {
        e.preventDefault()
        let email = this.state.email.trim()
        let password = this.state.password.trim()
        let confirmPassword = this.state.confirmPassword.trim()
        let name = this.state.name.trim()
        if (!email || !password || !confirmPassword || !name) {
            return
        }
        MyAjax.signupRequestSubject.next({
            auto_login: false,
            connection: 'Username-Password-Authentication',
            email: email,
            password: password,
            user_metadata: {
                name: name,
                gender: this.state.gender.trim() === 'true'
            }
        })
    },
    isSignUp() {
        let email = this.state.email.trim()
        let password = this.state.password.trim()
        let confirmPassword = this.state.confirmPassword.trim()
        let name = this.state.name.trim()
        return email && password && confirmPassword && name
            && password === confirmPassword
    },
    goToLogin(e) {
        router.navigate('login')
    },
    render() {
        return (
            <form className='form-content' onSubmit={this.handleSubmit}>
                <TextField
                    floatingLabelText='Email'
                    value={this.state.email}
                    onChange={this.handleChange('email')}
                    />
                <TextField
                    floatingLabelText='Password'
                    value={this.state.password}
                    onChange={this.handleChange('password')}
                    type='password'
                    />
                <TextField
                    floatingLabelText='Confirm Password'
                    value={this.state.confirmPassword}
                    onChange={this.handleChange('confirmPassword')}
                    type='password'
                    />
                <TextField
                    floatingLabelText='Name'
                    value={this.state.name}
                    onChange={this.handleChange('name')}
                    />
                <RadioButtonGroup className='row-content radio'
                    name='gender' defaultSelected='true'
                    onChange={this.handleChange('gender')}>
                    <RadioButton
                        style={{maxWidth: '50%'}}
                        value='true'
                        label='Male'
                        />
                    <RadioButton
                        style={{maxWidth: '50%'}}
                        value='false'
                        label='Female'
                        />
                </RadioButtonGroup>
                <CardActions className='row-content'>
                    <RaisedButton
                        label='Sign Up'
                        primary={true} className='mdl-button-50-per' type='submit'
                        disabled={!this.isSignUp()}>
                    </RaisedButton>
                    <RaisedButton
                        label='Back'
                        primary={true} className='mdl-button-50-per' onMouseDown={this.goToLogin} >
                    </RaisedButton>
                </CardActions>
            </form>
        )
    }
})

export default React.createClass({
    mixins: [MeterialMixin],
    render() {
        return (
            <section className='sign-up-container'>
                <Card>
                    <CardTitle><span className='title'>Sign Up</span></CardTitle>
                    <CardText>
                        <SignUpForm />
                    </CardText>
                </Card>
            </section>
        )
    }
})

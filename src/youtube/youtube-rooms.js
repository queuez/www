import _ from 'lodash'

import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import { Link } from 'react-router5'
import React from 'react'

import {socket} from './../lib/notification'
import {MeterialMixin} from './../lib/my-meterial-ui'
import {ReactorMixin} from './../lib/reactor'
import {router, routes} from './../lib/router'

export default React.createClass({
    mixins: [MeterialMixin, ReactorMixin],
    getInitialState() {
        return {roomName: '', youtubeRooms: {}}
    },
    /*
        socket on
    */
    onRooms(response) {
        this.setState({youtubeRooms: response.youtubeRooms})
    },
    componentWillMount() {
        socket.on('rooms', this.onRooms)
    },
    componentDidMount() {
        socket.emit('rooms')
    },
    componentWillUnmount() {
        socket.removeListener('rooms', this.onRooms)
    },
    handleRoomName(e) {
        this.setState({roomName: e.target.value})
    },
    handleCreateRoom(e) {
        e.preventDefault()
        let roomName = this.state.roomName.trim();
        if (!roomName) {
           return;
        }
        roomName = roomName.toLowerCase()
        .replace(/ /g,'-')
        .replace(/_/g,'-')
        .replace(/[^\w-]+/g,'')
        router.navigate('home.youtubeRoom', {room: roomName}, {reload: false})
    },
    render: function() {
        let componentRooms = () => {
                return _(this.state.youtubeRooms)
                .keys()
                .filter((youtube) => this.state.youtubeRooms[youtube].numbers > 0)
                .map((youtube, index) => {
                    return (
                        <div key={index}>
                            <span><Link routeName='home.youtubeRoom' routeParams={{room: youtube}} >{youtube} ({this.state.youtubeRooms[youtube].numbers} คน)</Link></span>
                        </div>
                    );
                }).value()
        }
        return (
            <Card className='youtube-rooms-container'>
                <form className='create-room-container' onSubmit={this.handleCreateRoom}>
                    <TextField
                        className='textfield-room'
                        floatingLabelText='Create Room'
                        value={this.state.roomName}
                        onChange={this.handleRoomName}
                        />
                    <RaisedButton
                        label='Create Room'
                        primary={true} className='button-create-room' type='submit'
                        >
                    </RaisedButton>
                </form>
                <section className='rooms-container'>
                    <h3>Rooms</h3>
                    {componentRooms()}
                </section>
            </Card>
        );
    }
})

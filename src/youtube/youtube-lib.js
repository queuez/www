import lodash from 'lodash';
let _ = lodash;

/***
    *(object[], object[]) => object[]
    [e, f, g], [a, b, c, d, e, i] => [e, i]
***/
let syncListYouTube = (oldList, newList) => {
    // 4
    let firstIndexMore = _.findIndex(newList, function(l) { return  l.id >= _.head(oldList).id });
    // [e, i]
    return _.drop(newList, firstIndexMore)
}

let hasIdYoutubeURL = (url) => {
    var regexp = /https:\/\/www\.youtube\.com\/watch\?v=[_\-a-zA-Z0-9]{0,11}/;
    var matches_array = url.match(regexp);
    if(matches_array !== null && matches_array.length > 0) {
        return matches_array[0].split('?v=')[1];
    }
    return null
}

function onPlayerReady(callback) {
    return (event) => {
        var videoData = event.target.getVideoData();
        callback(event.target, videoData)
    }
}

let getYoutubeTitle = (idHTMLTag, idYoutube, callback) => {
    let bufferPlayer = new YT.Player(idHTMLTag, {
        videoId: idYoutube,
        events: {
            'onReady': onPlayerReady((event, videoData) => {
                callback({
                    title: videoData['title']
                })
                bufferPlayer.destroy()
            })
        }
    });
}

export {
    syncListYouTube,
    getYoutubeTitle,
    hasIdYoutubeURL
}

import _ from 'lodash'
import moment from 'moment'
import React from 'react'

/**
    * Material UI
    */
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card'
import IconButton from 'material-ui/IconButton'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'

/**
    * Material UI Icon
    */
import AvMicOff from 'material-ui/svg-icons/av/mic-off'
import AvSkipNext from 'material-ui/svg-icons/av/skip-next'
import AvStop from 'material-ui/svg-icons/av/stop'
import AvVideoLibrary from 'material-ui/svg-icons/av/video-library'
import HardwareHeadsetMic from 'material-ui/svg-icons/hardware/headset-mic'

import {myCookie} from './../lib/cookie'
import {socket} from './../lib/notification'
import {MeterialMixin} from './../lib/my-meterial-ui'
import {ReactorMixin} from './../lib/reactor'
import {router} from './../lib/router'
import {objectToStyle} from './../lib/style'
import {getYoutubeTitle, hasIdYoutubeURL} from './youtube-lib'
import YouTubePlayer from 'youtube-player'

let player

let generate_slug = () => {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return `${text}_${Date.now()}`;
}

export default React.createClass({
    mixins: [MeterialMixin, ReactorMixin],
    getInitialState() {
        let requestName = "No name"
        if(myCookie.load() && myCookie.load().requestName) {
            requestName = myCookie.load().requestName
        }
        return {
            listYoutube:[] ,
            listSearchYoutube: [],
            request: requestName,
            play: {},
            listIndexYoutube: -1,
            isCancelPlayByDJ: false,
            isListenYoutube: false,
            youtubeLink: '',
            youtubeSearch: '',
            isYoutubeLink: false,
            canPlayWithDJ: false,
            isDJ: false,
            permission: {
                canPlay: true
            }
        }
    },
    /*
        socket on
    */
    onSyncListYoutube(response) {
        if(_.isEqual(this.state.play, {})) {
            this.skipYoutube(response.listYoutube, 10)
        }
        this.setState({listYoutube: response.listYoutube})
    },
    onConnectJoinYoutube () {
        console.log('====================');
        socket.emit('joinYoutube', {
            roomName: this.props.params.room
        })
    },
    onJoinYoutube(response) {
        if(!socket.hasListeners('connectJoinYoutube')) socket.on('connectJoinYoutube', this.onConnectJoinYoutube)
        this.setState({
            canPlayWithDJ: response.canPlayWithDJ,
            permission: response.permission,
            listYoutube: response.listYoutube,
            isJoinYoutube: false
        })
    },
    onLoginDJ(response) {
        if(response.permission) {
            //For DJ
            this.setState({
                permission: response.permission,
                canPlayWithDJ: false,
                isDJ: true,
            })
            this.skipYoutube(this.state.listYoutube, 10)
        } else {
            //Another
            this.setState({
                canPlayWithDJ: false,
                isDJ: false,
            })
        }
    },
    onLogoutDJ(response) {
        if(response.permission) {
            //For DJ
            this.setState({
                permission: response.permission,
                canPlayWithDJ: true,
                isDJ: false,
                isListenYoutube: false
            })
        } else {
            //Another
            this.setState({
                canPlayWithDJ: true,
                isDJ: false,
                isListenYoutube: false
            })
        }
    },
    onSearchYoutube(response) {
        if(response.listSearchYoutube) {
            //For DJ
            this.setState({
                listSearchYoutube: response.listSearchYoutube
            })
        }
    },
    componentWillMount() {

        socket.on('syncListYoutube', this.onSyncListYoutube)

        socket.on('joinYoutube', this.onJoinYoutube)

        socket.on('loginDJ', this.onLoginDJ)

        socket.on('logoutDJ', this.onLogoutDJ)

        socket.on('searchYoutube', this.onSearchYoutube)
    },
    componentDidMount() {
        socket.emit('joinYoutube', {
            roomName: this.props.params.room
        })
        player = YouTubePlayer('player',{
            playerVars: {
                'autoplay': 1,
                'controls': 0,
                'rel' : 0,
                'showinfo': 0
            }
        })

        player.on('stateChange', (event) => {

            if(event.data === 0) {
                if(!this.state.canPlayWithDJ && this.state.isListenYoutube
                    && !this.state.isDJ) {
                    let listYoutube = this.state.listYoutube
                    let index = _.findIndex(listYoutube, (youtube) => {
                        this.state.play.slugYoutube === youtube.slugYoutube
                    })
                    if(index !== -1) {
                        let youtube = listYoutube[index + 1]
                        player.loadVideoById(youtube.id);
                        player.pauseVideo();
                        this.state.play = youtube
                        this.setState({listIndexYoutube: index + 1})
                    }
                } else if(this.state.isDJ) {
                    this.skipYoutube(this.state.listYoutube)
                }
            }
            else if(event.data === 3) {
                event.target.playVideo()
            }
            /*else if(event.data === 1) {
                this.state.play.title = event.target.getVideoData()['title']
                this.setState({play: this.state.play})
            }*/
        })
    },
    componentWillUnmount() {
        socket.removeListener('syncListYoutube',this.onSyncListYoutube)
        socket.removeListener('connectJoinYoutube',this.onConnectJoinYoutube)
        socket.removeListener('joinYoutube', this.onJoinYoutube)
        socket.removeListener('loginDJ', this.onLoginDJ)
        socket.removeListener('logoutDJ', this.onLogoutDJ)
        socket.removeListener('searchYoutube', this.onSearchYoutube)
    },
    handleLoginDJ(e) {
        this.loginDJ()
    },
    handleLogoutDJ(e) {
        this.logoutDJ()
    },
    handleListenYoutube(e) {
        this.listenYoutube()
    },
    handleStopYoutube(e) {
        this.stopYoutube()
    },
    handleSkipYoutube(e) {
        this.skipYoutube(this.state.listYoutube)
    },
    handleYoutubeLink(e) {
        this.setState({youtubeLink: e.target.value})
    },
    handleRequest(e) {
        myCookie.save({requestName: e.target.value})
        this.setState({request: e.target.value})
    },
    handleSearchMusic(e) {
        this.setState({youtubeSearch: e.target.value})
    },
    handleSendMusicLink(e) {
        e.preventDefault()
        let youtubeLink = this.state.youtubeLink.trim();
        if (!youtubeLink) {
           return;
        }
        let idYoutube = hasIdYoutubeURL(youtubeLink)

        if(idYoutube) {
            getYoutubeTitle('youtube', idYoutube, (data) => {
                if(data.title !== '') {
                    socket.emit('sendMusic', {
                        slugYoutube: generate_slug(),
                        name: this.state.request,
                        idex: 1,
                        //name: myCookie.load().name,
                        //idex: myCookie.load().id,
                        title: data.title,
                        idYoutube: idYoutube
                    });
                    if(this.state.isYoutubeLink)
                        this.setState({isYoutubeLink: false})

                } else if(!this.state.isYoutubeLink) {
                    this.setState({isYoutubeLink: true})
                }
            })
            this.setState({youtubeLink: ''})
        } else if(!this.state.isYoutubeLink) {
            this.setState({youtubeLink: '', isYoutubeLink: true})
        } else {
            this.setState({youtubeLink: ''})
        }
    },
    handleSendMusic(idYoutube, title) {
        return (e) => {
            socket.emit('sendMusic', {
                slugYoutube: generate_slug(),
                name: this.state.request,
                idex: 1,
                //name: myCookie.load().name,
                //idex: myCookie.load().id,
                title: title,
                idYoutube: idYoutube
            })
        }
    },
    handleSubmitMusic(e) {
        e.preventDefault()
        socket.emit('searchYoutube', {
            name: this.state.youtubeSearch
        })
    },
    showListButtonForPlay() {
        if(this.state.permission.canPlay && (this.state.canPlayWithDJ || this.state.isDJ)) {
            return (
                <div className='list-button-for-play'>
                    <IconButton className={
                            objectToStyle({
                                'hide': !this.state.canPlayWithDJ || this.state.isDJ
                            })
                            }
                            style={{width: 36, height: 36, padding: 8}}
                            tooltip='DJ'
                            touch={true} tooltipPosition='top-center'
                            onMouseDown={this.handleLoginDJ}
                        >
                        <HardwareHeadsetMic/>
                    </IconButton>
                    <IconButton className={
                            objectToStyle({
                                'hide': !this.state.isDJ
                            })
                            }
                            style={{width: 36, height: 36, padding: 8}}
                            tooltip='DJ off'
                            touch={true} tooltipPosition='top-center'
                            onMouseDown={this.handleLogoutDJ}
                        >
                        <AvMicOff/>
                    </IconButton>
                    <IconButton className={
                            objectToStyle({
                                'hide': !this.state.permission.canSkip
                            })
                            }
                            style={{width: 36, height: 36, padding: 8}}
                            tooltip='Skip'
                            touch={true} tooltipPosition='top-center'
                            onMouseDown={this.handleSkipYoutube}
                        >
                        <AvSkipNext/>
                    </IconButton>
                </div>
            )
        } else if(this.state.permission.canPlay && !this.state.canPlayWithDJ && !this.state.isDJ) {
            return (
                <div className='list-button-for-play'>
                    <IconButton className={
                            objectToStyle({
                                'hide': this.state.isListenYoutube
                            })
                            }
                            style={{width: 36, height: 36, padding: 8}}
                            tooltip='Listen to music'
                            touch={true} tooltipPosition='top-center'
                            onMouseDown={this.handleListenYoutube}
                        >
                        <AvVideoLibrary/>
                    </IconButton>
                    <IconButton className={
                            objectToStyle({
                                'hide': !this.state.isListenYoutube
                            })
                            }
                            style={{width: 36, height: 36, padding: 8}}
                            tooltip='Stop'
                            touch={true} tooltipPosition='top-center'
                            onMouseDown={this.handleStopYoutube}
                        >
                        <AvStop/>
                    </IconButton>
                </div>
            )
        } else {
            return (<div></div>)
        }
    },
    loginDJ() {
        socket.emit('loginDJ', {})
    },
    logoutDJ() {
        socket.emit('logoutDJ', {})
        this.setState({isCancelPlayByDJ: false})
    },
    listenYoutube() {
        if(!this.state.canPlayWithDJ && !this.state.isListenYoutube
            && !this.state.isDJ) {
            let index = 9
            let youtube = this.state.listYoutube[index + 1]
            player.loadVideoById(youtube.id);
            player.pauseVideo();
            this.state.play = youtube
            player.playVideo()
            this.setState({
                listIndexYoutube: index + 1,
                isListenYoutube: true
            })
        }
    },
    stopYoutube() {
        player.stopVideo()
        this.setState({isListenYoutube: false})
    },
    skipYoutube(listYoutube, index = 11) {
        if(this.state.isDJ) {
            if(listYoutube[index] &&
                listYoutube[index].slugYoutube !== this.state.play.slugYoutube) {
                player.loadVideoById(listYoutube[index].id);
                player.pauseVideo();
                this.state.play = listYoutube[index]

                if(index !== 11) {
                    socket.emit('youtubeDJ', {
                        slugYoutube: this.state.play.slugYoutube
                    })
                } else {
                    socket.emit('youtubeDJ', {
                        startNewMusic: true,
                        slugYoutube: this.state.play.slugYoutube
                    })
                }
                if(this.state.isListenYoutube === false) {
                    this.setState({isListenYoutube: true, play: this.state.play})
                } else {
                    this.setState({play: this.state.play})
                }
            } else {
                if(listYoutube.length === 11) {
                    socket.emit('youtubeDJ', {
                        endMusic: true,
                        slugYoutube: this.state.play.slugYoutube
                    })
                }
                this.setState({play: {}})
            }
        }
    },
    render() {
        let componentListYoutube = () => {
            if(!this.state.isListenYoutube && !this.state.isDJ) {
                return _(this.state.listYoutube)
                .filter((youtube, index) => index > 9)
                .map((youtube, index) => {
                    return (
                        <div key={index}>
                            <span>{index+1}. {youtube.title}   ขอโดย {youtube.name}</span>
                        </div>
                    );
                }).value()
            } else if(this.state.isListenYoutube && !this.state.isDJ) {
                return _(this.state.listYoutube)
                .filter((youtube, index) => index > this.state.listIndexYoutube)
                .map((youtube, index) => {
                    return (
                        <div key={index}>
                            <span>{index+1}. {youtube.title}   ขอโดย {youtube.name}</span>
                        </div>
                    );
                }).value()
            } else {
                return _(this.state.listYoutube)
                .filter((youtube, index) => index > 9)
                .tail().map((youtube, index) => {
                    return (
                        <div key={index}>
                            <span>{index+1}. {youtube.title}   ขอโดย {youtube.name}</span>
                        </div>
                    );
                }).value()
            }
        }

        let componentListSearchYoutube = () => {
                return _(this.state.listSearchYoutube)
                .map((youtube, index) => {
                    return (
                        <div key={index} className='youtube-detail-search-container'
                            onMouseDown={this.handleSendMusic(youtube.id, youtube.title)}>
                            <img src={youtube.thumbnails.medium.url}
                                className='image'/>
                            <div className='youtube-detail-search-name-time-container'>
                                <span>{youtube.title}</span>
                                <span className='time'>
                                    {moment(youtube.publishedAt).fromNow()}
                                </span>
                            </div>
                        </div>
                    );
                }).value()
        }

        return (
        <section className='youtube-container'>
            <section className='youtube-mian-detail-container'>
                <section className='youtube-detail-container'>
                    <section id='youtube'></section>
                    <section id='player'></section>
                    <section>
                        <div>
                            <strong>เพลง: </strong><span>{this.state.play.title}</span>
                        </div>
                        <div>
                            <strong>ขอโดย: </strong><span>{this.state.play.name}</span>
                        </div>
                        {this.showListButtonForPlay()}
                    </section>
                    <section>
                        <strong>รายการเพลง</strong>
                        {componentListYoutube()}
                    </section>
                </section>
                <section className='youtube-playlist-search-container'>
                    <form className='youtube-playlist-container' onSubmit={this.handleSendMusicLink}>
                        <TextField
                            className='textfield-youtube-link'
                            floatingLabelText='Request By'
                            value={this.state.request}
                            onChange={this.handleRequest}
                            />
                        <TextField
                            className='textfield-youtube-link'
                            floatingLabelText='Youtube Link'
                            value={this.state.youtubeLink}
                            onChange={this.handleYoutubeLink}
                            />
                            <div className={
                                    objectToStyle({
                                        'hide': !this.state.isYoutubeLink
                                    })
                                }>
                                <span>Not youtube link</span>
                            </div>
                            <div className='button-youtube-link-container'>
                                <RaisedButton
                                    label='Send Music Link'
                                    primary={true} className='button-youtube-link' type='submit'
                                    >
                                </RaisedButton>
                            </div>
                    </form>
                    <form className='youtube-playlist-container' onSubmit={this.handleSubmitMusic}>
                        <TextField
                            className='textfield-youtube-link'
                            floatingLabelText='Search'
                            value={this.state.youtubeSearch}
                            onChange={this.handleSearchMusic}
                            />
                            <div className='button-youtube-link-container'>
                                <RaisedButton
                                    label='Search'
                                    primary={true} className='button-youtube-link' type='submit'
                                    >
                                </RaisedButton>
                            </div>
                    </form>
                    <section className='youtube-list-container'>
                        <strong>รายการค้นหา</strong>
                        {componentListSearchYoutube()}
                    </section>
                </section>
            </section>
        </section>
        );
    }
})

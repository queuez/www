import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import React from 'react'

const MeterialMixin = {
    childContextTypes: {
        muiTheme: React.PropTypes.object.isRequired,
    },
    getChildContext() {
        return {muiTheme: getMuiTheme(baseTheme)};
    }
}

export {
    MeterialMixin
}

import _ from 'lodash'
import $ from 'jquery'
import cookie from 'react-cookie'
import Rx from '@reactivex/rxjs'

import {myCookie} from './cookie'
import {iqzAuth} from './auth/auth-service'

let ajax = (options, isAuthorization) => {
    let defaultOptions = {
        contentType: 'application/json',
        dataType: 'json',
        beforeSend (xhr) {
            if(isAuthorization === true) {
                xhr.setRequestHeader('Authorization', 'Token ' + myCookie.load().token)
                xhr.setRequestHeader('X-CSRFToken', cookie.load('csrftoken'))
            }
        },
    }
    let newOptions = _.merge(defaultOptions, options)
    return $.ajax(newOptions)
}

let objectToFormData = (data) => {
    let fd = new FormData();
    _.forEach(data, function(value, key) {
        fd.append(key, value);
    });
    return fd;
}

/*
    * Signup
    */
let signup = (data) => {
    return Rx.Observable.create(observer => {
        iqzAuth.signup(data, (err) => {
            if(err) {
                console.error('Signup error', err)
            } else {
                observer.next({
                    status: 'success'
                })
                observer.complete()
            }
        })
        // let formData = objectToFormData(data)
        // return ajax({
        //     processData: false,
        //     contentType: false,
        //     cache: false,
        //     url: `b/app/signup/`,
        //     type: 'POST',
        //     data: formData
        // })
        // .then((data, status) => {
        //     data.status = status
        //     observer.next(data)
        //     observer.complete()
        // })
    })
}

const signupRequestSubject = new Rx.Subject()
const signupResponse = signupRequestSubject
    .switchMap(signup)
    .share()

const signupSuccess = signupResponse
    .filter((response) => response.status === 'success')

/*
    * Login
    */

let login = ({email, password}) => {
    return Rx.Observable.create(observer => {
        iqzAuth.login({
            connection: 'Username-Password-Authentication',
            responseType: 'token',
            email: email,
            password: password
        }, (err, result) => {
            if (err) {
                console.error('Login error', err);
            } else {
                iqzAuth.setToken(result.idToken)
                observer.next({
                    data: result,
                    status: 'success'
                })
                observer.complete()
            }
        })
        // ajax({
        //     url: `b/app/login/`,
        //     type: 'POST',
        //     data: JSON.stringify({
        //         email: data.email,
        //         password: data.password
        //     })
        // })
        // .then((data, status) => {
        //     data.status = status
        //     observer.next(data)
        //     observer.complete()
        // })
    })
}

const loginRequestSubject = new Rx.Subject()
const loginResponse = loginRequestSubject
    .switchMap(login)
    .share()

const loginSuccess = loginResponse
    .filter((response) => response.status === 'success')

/*
    * Logout
    */

const logout = () => {
    return Rx.Observable.create(observer => {
        iqzAuth.logout()
        observer.next({status: 'success'})
        observer.complete()
    })
}
const logoutRequestSubject = new Rx.Subject()
const logoutResponse = logoutRequestSubject
    .filter((status) => status === 'logout')
    .switchMap(logout)
    .share()

const logoutSuccess = logoutResponse
    .filter((response) => response.status === 'success')

export {
    ajax,
    signupRequestSubject,
    signupSuccess,
    loginRequestSubject,
    loginSuccess,
    logoutRequestSubject,
    logoutSuccess,
    objectToFormData
}

/*
n searchWikipedia (term) {
    var promise = $.ajax({
        url: 'http://en.wikipedia.org/w/api.php',
        dataType: 'jsonp',
        data: {
            action: 'opensearch',
            format: 'json',
            search: encodeURI(term)
        }
    }).promise();
    return Rx.Observable.fromPromise(promise);
*/

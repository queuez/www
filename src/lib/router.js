import Rx from '@reactivex/rxjs'
import {Router5} from 'router5'
import listenersPlugin from 'router5-listeners'
import historyPlugin from 'router5-history'

import {iqzAuth} from './auth/auth-service'

const routesSimple = [{ name: 'home', path: '/app', children: [
    {name: 'chats', path: '/chats'},
    {name: 'youtubeRooms', path: '/youtube-rooms'},
    {name: 'youtubeRoom', path: '/youtube-room/:room'}
]}]

const router = new Router5(routesSimple, {
        useHash: false,
        defaultRoute: 'home'
    })

router.add({ name: 'login', path: '/login' })
router.add({ name: 'signup', path: '/signup' })
router.add({ name: 'chats', path: '/chats' })
//router.add({ name: 'youtube', path: '/youtube' })

router.usePlugin(listenersPlugin())
router.usePlugin(historyPlugin())

// prevent unauthenticated user from auth zone
router.canActivate('home', (toState, fromState) => {
    if(!iqzAuth.loggedIn()) {
        return Promise.reject({ redirect: { name: 'login' } })
    } else {
        return true
    }
})

router.canActivate('login', () => {
    if(iqzAuth.loggedIn()) {
        return Promise.reject({ redirect: { name: 'home' } })
    } else {
        return true
    }
})

router.canActivate('signup', () => {
    if(iqzAuth.loggedIn()) {
        return Promise.reject({ redirect: { name: 'home' } })
    } else {
        return true
    }
})

const routeActivatedSubject = new Rx.Subject()
router.addListener((to, from) => {
    routeActivatedSubject.next({to, from})
})

const routeActivated = routeName => {
    return routeActivatedSubject.filter(() => {
        if(_.isEqual(router.getState().params, {})) {
            return router.isActive(routeName)
        } else {
            return router.isActive(routeName, router.getState().params)
        }
    }).asObservable()
}

const routes = {
    home: {
        activated: routeActivated('home'),
        chats: {
            activated: routeActivated('home.chats')
        },
        youtubeRooms: {
            activated: routeActivated('home.youtubeRooms')
        },
        youtubeRoom: {
            activated: routeActivated('home.youtubeRoom')
        }
    },
    login: {
        activated: routeActivated('login')
    },
    signup: {
        activated: routeActivated('signup')
    },
}

export {
    router,
    routes
}

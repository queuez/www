import _ from 'lodash'

let arrayToStyle = (array) => {
    return _(array).filter((values, nameClass) => {
        return values[1]
        if(values[1]) {
            styles.push(nameClass)
        }
    }).reduce((styles, values) => {
        return `${styles} ${values[0]}`
    }, '')
}

let objectToStyle = (objStyle) => {
    return _.reduce(objStyle, (styles, value, nameClass) => {
        if(value) {
            styles = `${nameClass} ${styles}`
        }
        return styles;
    }, '');
}

export {arrayToStyle, objectToStyle}

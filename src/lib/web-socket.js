const WebSocketMixin = {
    componentWillMount() {
        this.wss = []
    },
    onWs(ws) {
        this.wss.push(ws)
    },
    componentWillUnmount() {
        this.wss.forEach(ws => ws.close())
    }
}

export {
    WebSocketMixin
}

//
// ws.onopen = function()
//                 {
//                 // Web Socket is connected, send data using send()
//                 ws.send(JSON.stringify({
//                     room: "ben"
//                 }));
//                 console.log("Message is sent...");
//                };
//
//                ws.onmessage = function (evt)
//                {
//                   var received_msg = evt.data;
//                   console.log("Message is received...");
//                };
//
//                ws.onclose = function()
//                {
//                   // websocket is closed.
//                   console.log("Connection is closed...");
//                };

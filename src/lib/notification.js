import $ from 'jquery'
import lodash from 'lodash';
let _ = lodash;


const socket = {}//io(`https://${location.hostname}:8080`);

const ReactorMixin = {
    componentWillMount() {
        this.subscriptions = []
    },
    on(observable, reaction) {
        this.subscriptions.push(observable.subscribe(reaction))
    },
    componentWillUnmount() {
        this.subscriptions.forEach(s => s.unsubscribe())
    }
}

export {
    socket,
    ReactorMixin
}

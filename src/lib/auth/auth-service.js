import _ from 'lodash'

import Auth0 from 'auth0-js'
import decode from 'jwt-decode'
import { EventEmitter } from 'events'

const AUTH0_CLIENT_ID = 'ZUgmf4sAw5IO2zzii0vdUAm7S8JcQjaG'
const AUTH0_DOMAIN = 'iqueuez.auth0.com'
const CALLBACK_URL = location.origin

export default class AuthService extends EventEmitter {
    constructor(clientId, domain) {
        super()
        // Configure Auth0
        this.auth0 = new Auth0({
            clientID: clientId,
            domain: domain,
            responseType: 'token',
            callbackURL:  CALLBACK_URL
        })

        this.login = this.login.bind(this)
        this.signup = this.signup.bind(this)
    }

    login(params, onError) {
        //redirects the call to auth0 instance
        this.auth0.login(params, onError)
    }

    signup(params, onError) {
        //redirects the call to auth0 instance
        this.auth0.signup(params, onError)
    }

    parseHash(hash) {
        // uses auth0 parseHash method to extract data from url hash
        const authResult = this.auth0.parseHash(hash)
        if (authResult && authResult.idToken) {
            this.setToken(authResult.idToken)
        }
    }

    loggedIn() {
        // Checks if there is a saved token and it's still valid
        const token = this.getToken()
        return !!token && !isTokenExpired(token)
    }

    setToken(idToken) {
        // Saves user token to localStorage
        this.auth0.getProfile(idToken, (err, profile) => {
            if (err) {
                console.error('Error loading the Profile', err)
            } else {
                this.setProfile(profile)
            }
        })
        localStorage.setItem('id_token', idToken)
    }

    getToken() {
        // Retrieves the user token from localStorage
        return localStorage.getItem('id_token')
    }

    setProfile(profile) {
        // Saves profile data to localStorage
        console.log('setProfile', profile);
        localStorage.setItem('profile', JSON.stringify(profile))
        // Triggers profile_updated event to update the UI
        this.emit('profile_updated', profile)
    }

    getProfile() {
        // Retrieves the profile data from localStorage
        const profile = localStorage.getItem('profile')
        return profile ? JSON.parse(localStorage.profile) : {}
    }

    getUserID() {
        // Retrieves the profile data from localStorage
        const profile = this.getProfile()
        return _.get(profile, 'user_id', '').split('|')[1]
    }

    isAdmin() {
        // Checks if user have `admin` role in his profile app_metadata
        const profile = this.getProfile();
        const { roles } = profile.app_metadata || {};
        return !!roles && roles.indexOf('admin') > -1;
    }

    logout() {
        // Clear user token and profile data from localStorage
        localStorage.removeItem('id_token');
    }
}

export const getTokenExpirationDate = (token) => {
    const decoded = decode(token)
    if(!decoded.exp) {
        return null
    }

    const date = new Date(0) // The 0 here is the key, which sets the date to the epoch
    date.setUTCSeconds(decoded.exp)
    return date
}

export const isTokenExpired = (token) => {
    const date = getTokenExpirationDate(token)
    const offsetSeconds = 0
    if (date === null) {
        return false
    }
    return !(date.valueOf() > (new Date().valueOf() + (offsetSeconds * 1000)))
}

const iqzAuth = new AuthService(AUTH0_CLIENT_ID, AUTH0_DOMAIN);

export {
    iqzAuth
}

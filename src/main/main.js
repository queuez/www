import $ from 'jquery'
window.jQuery = window.$ = $

import _ from 'lodash'
import React from 'react'
import Textarea from 'react-textarea-autosize';

/**
    * Material UI
    */
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

/**
    * Material UI Icon
    */
import AvMusicVideo from 'material-ui/svg-icons/av/music-video'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import * as MyAjax from './../lib/my-ajax'
import {MeterialMixin} from './../lib/my-meterial-ui'
import {ReactorMixin} from './../lib/reactor'
import {router, routes, openFirst} from './../lib/router'
//import UploadFileMenu from './../my-button/upload-file-menu'
import {arrayToStyle} from './../lib/style'
import YoutubeRoom from './../youtube/youtube-room'
import YoutubeRooms from './../youtube/youtube-rooms'
import Chats from './../chat/chat'

const Apps = React.createClass({
    getInitialState() {
    let apps = [{
        name: 'Youtube',
        routeName: 'home.youtubeRooms',
        icon: <AvMusicVideo/>
    }]
    return {apps: apps, content: ''}
    },
    handleApp(routeName) {
        return (e) => {
            router.navigate(routeName, {}, {reload: true})
        }
    },
    render() {
        let componentApps = () => {
                return _(this.state.apps)
                .map((app, index) => {
                    return (
                        <div key={index} className='app-button-container'>
                            <IconButton
                                    iconStyle={{width: 48, height: 48}}
                                    style={{width: 64, height: 64, padding: 8}}
                                    touch={true}
                                    onMouseDown={this.handleApp(app.routeName)}
                                >
                                {app.icon}
                            </IconButton>
                            <strong>
                                {app.name}
                            </strong>
                        </div>
                    );
                }).value()
        }
        return (
            <section className='app-container'>
                {componentApps()}
            </section>
        )
    }
})

let querySubStateMain = (pathname) => {
    let pathnames = location.pathname.split('/')
    if(_.isEqual(pathnames, ['', 'app', 'chats'])) {
        router.navigate('home.chats', {}, {reload: true})
    } else if(_.isEqual(pathnames, ['', 'app', 'youtube-rooms'])) {
        router.navigate('home.youtubeRooms', {}, {reload: true})
    } else if(_.isEqual(pathnames, ['', 'app' ,'youtube-room', pathnames[3]])) {
        let room = pathnames[3].toLowerCase()
        .replace(/ /g,'-')
        .replace(/_/g,'-')
        .replace(/[^\w-]+/g,'')
        router.navigate('home.youtubeRoom', {room: room}, {reload: true})
    }
}

export default React.createClass({
    mixins: [MeterialMixin, ReactorMixin],
    getInitialState() {
        return {content: ''}
    },
    componentWillMount() {
        this.on(MyAjax.logoutSuccess, () => {
            router.navigate('login')
        })

        if(router.isActive('home')) {
            this.setState({ content: <Apps/> })
        }
        this.on(routes.home.activated, () => {
            this.setState({ content: <Apps/> })
        })
        this.on(routes.home.chats.activated, () => {
            this.setState({ content: <Chats/> })
        })
        this.on(routes.home.youtubeRooms.activated, () => {
            this.setState({ content: <YoutubeRooms/> })
        })
        this.on(routes.home.youtubeRoom.activated, ({to}) => {
            this.setState({ content: <YoutubeRoom params={to.params}/> })
        })
        querySubStateMain(location.pathname)
    },
    handleLogout(e) {
        MyAjax.logoutRequestSubject.next('logout')
    },
    render() {
        return (
        <section className='main-container'>
            <AppBar
                title="iQueueZ"
                iconElementLeft={<span></span>}
                iconElementRight={
                    <IconMenu
                            iconButtonElement={
                                <IconButton>
                                    <MoreVertIcon />
                                </IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                        >
                        <MenuItem primaryText="Logout" onMouseDown={this.handleLogout} />
                  </IconMenu>
                }
                />
                <section className='row-flex'>
                    {this.state.content}
                </section>
        </section>
        );
    }
})
/*<Queue/>*/
